![image](image/cover.jpg "Welcome Image")

## Introduction

Gaussian Distribution is a probability distribution that is symmetric about the mean, showing that data near the mean are more frequent in occurrence than data far from the mean. In graph form, **normal distribution** will appear as a bell curve.

> The distribution fits the probability distribution of many events, eg. IQ Scores, Heartbeat etc.

## Using Python

Use the `random.normal()` method to get a Normal Data Distribution, which has three parameters:

- `loc` - (Mean) where the peak of the bell exists.
- `scale` - (Standard Deviation) how flat the graph distribution should be.
- `size` - The shape of the returned array.

#### Example

Generate a random normal distribution of size 2x3:

```python
from numpy import random
x = random.normal(size=(2, 3))
print(x)
```

## About Gaussian Distribution

The probability density function of the distribution is as below:

![image](image/pdf.svg "Probability Density Function")

The following are the parameters of the Gaussian distribution:

|Name | Value |
|-----|----|
|Mean |  $`\mu`$ |
|Median | $`\mu`$ |
|Mode | $`\mu`$ |
|Variance | $`0`$ |
|Entropy | $`\dfrac{1}{2}\log (2\pi e \sigma^2)`$ |

### Further Readings

1. Bryc, Wlodzimierz (1995). The Normal Distribution: Characterizations with Applications. Springer-Verlag. [ISBN 978-0-387-97990-8](https://en.wikipedia.org/wiki/Special:BookSources/978-0-387-97990-8).
2. Aldrich, John; Miller, Jeff. ["Earliest Uses of Symbols in Probability and Statistics"](http://jeff560.tripod.com/stat.html)



